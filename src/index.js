const Account = [
    {
        username: 'mahdi',
        password: 'password'
    }
]


const verifyAccount = (payload) => {
    const fetchedData = Account.find((currentValue) => {
        if (payload.username.toLowerCase() === currentValue.username && payload.password === currentValue.password) {
            return true
        }
        return false
    })

    if (!fetchedData) {
        throw new Error('Autentikasi gagal')
    }

    return fetchedData
}

const getPayload = () => {
    const username = document.querySelector('#username').value
    const password = document.querySelector('#password').value
    return {
        username: username,
        password: password
    }
}

const loginButton = document.querySelector('#login')

loginButton.addEventListener('click', event => {
    const payload = getPayload()

    try {
        verifyAccount(payload)
        window.alert('Login successfully as mahdi')
    } catch (error) {
        window.alert('Login failed')
    }
})


